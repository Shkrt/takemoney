defmodule Takemoney.EventsTest do
  use Takemoney.DataCase

  alias Takemoney.Events

  describe "events" do
    alias Takemoney.Events.Event

    @valid_attrs %{description: "some description", image_url: "some image_url", name: "some name"}
    @update_attrs %{description: "some updated description", image_url: "some updated image_url", name: "some updated name"}
    @invalid_attrs %{description: nil, image_url: nil, name: nil}

    def event_fixture(attrs \\ %{}) do
      {:ok, event} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Events.create_event()

      event
    end

    test "list_events/0 returns all events" do
      event = event_fixture()
      assert Events.list_events() == [event]
    end

    test "get_event!/1 returns the event with given id" do
      event = event_fixture()
      assert Events.get_event!(event.id) == event
    end

    test "create_event/1 with valid data creates a event" do
      assert {:ok, %Event{} = event} = Events.create_event(@valid_attrs)
      assert event.description == "some description"
      assert event.image_url == "some image_url"
      assert event.name == "some name"
    end

    test "create_event/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Events.create_event(@invalid_attrs)
    end

    test "update_event/2 with valid data updates the event" do
      event = event_fixture()
      assert {:ok, event} = Events.update_event(event, @update_attrs)
      assert %Event{} = event
      assert event.description == "some updated description"
      assert event.image_url == "some updated image_url"
      assert event.name == "some updated name"
    end

    test "update_event/2 with invalid data returns error changeset" do
      event = event_fixture()
      assert {:error, %Ecto.Changeset{}} = Events.update_event(event, @invalid_attrs)
      assert event == Events.get_event!(event.id)
    end

    test "delete_event/1 deletes the event" do
      event = event_fixture()
      assert {:ok, %Event{}} = Events.delete_event(event)
      assert_raise Ecto.NoResultsError, fn -> Events.get_event!(event.id) end
    end

    test "change_event/1 returns a event changeset" do
      event = event_fixture()
      assert %Ecto.Changeset{} = Events.change_event(event)
    end
  end

  describe "performances" do
    alias Takemoney.Events.Performance

    @valid_attrs %{end_time: ~N[2010-04-17 14:00:00.000000], event_id: 42, start_time: ~N[2010-04-17 14:00:00.000000]}
    @update_attrs %{end_time: ~N[2011-05-18 15:01:01.000000], event_id: 43, start_time: ~N[2011-05-18 15:01:01.000000]}
    @invalid_attrs %{end_time: nil, event_id: nil, start_time: nil}

    def performance_fixture(attrs \\ %{}) do
      {:ok, performance} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Events.create_performance()

      performance
    end

    test "list_performances/0 returns all performances" do
      performance = performance_fixture()
      assert Events.list_performances() == [performance]
    end

    test "get_performance!/1 returns the performance with given id" do
      performance = performance_fixture()
      assert Events.get_performance!(performance.id) == performance
    end

    test "create_performance/1 with valid data creates a performance" do
      assert {:ok, %Performance{} = performance} = Events.create_performance(@valid_attrs)
      assert performance.end_time == ~N[2010-04-17 14:00:00.000000]
      assert performance.event_id == 42
      assert performance.start_time == ~N[2010-04-17 14:00:00.000000]
    end

    test "create_performance/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Events.create_performance(@invalid_attrs)
    end

    test "update_performance/2 with valid data updates the performance" do
      performance = performance_fixture()
      assert {:ok, performance} = Events.update_performance(performance, @update_attrs)
      assert %Performance{} = performance
      assert performance.end_time == ~N[2011-05-18 15:01:01.000000]
      assert performance.event_id == 43
      assert performance.start_time == ~N[2011-05-18 15:01:01.000000]
    end

    test "update_performance/2 with invalid data returns error changeset" do
      performance = performance_fixture()
      assert {:error, %Ecto.Changeset{}} = Events.update_performance(performance, @invalid_attrs)
      assert performance == Events.get_performance!(performance.id)
    end

    test "delete_performance/1 deletes the performance" do
      performance = performance_fixture()
      assert {:ok, %Performance{}} = Events.delete_performance(performance)
      assert_raise Ecto.NoResultsError, fn -> Events.get_performance!(performance.id) end
    end

    test "change_performance/1 returns a performance changeset" do
      performance = performance_fixture()
      assert %Ecto.Changeset{} = Events.change_performance(performance)
    end
  end

  describe "tickets" do
    alias Takemoney.Events.Ticket

    @valid_attrs %{access: 42, performance_id: 42, price: "120.5", status: 42, user_id: 42}
    @update_attrs %{access: 43, performance_id: 43, price: "456.7", status: 43, user_id: 43}
    @invalid_attrs %{access: nil, performance_id: nil, price: nil, status: nil, user_id: nil}

    def ticket_fixture(attrs \\ %{}) do
      {:ok, ticket} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Events.create_ticket()

      ticket
    end

    test "list_tickets/0 returns all tickets" do
      ticket = ticket_fixture()
      assert Events.list_tickets() == [ticket]
    end

    test "get_ticket!/1 returns the ticket with given id" do
      ticket = ticket_fixture()
      assert Events.get_ticket!(ticket.id) == ticket
    end

    test "create_ticket/1 with valid data creates a ticket" do
      assert {:ok, %Ticket{} = ticket} = Events.create_ticket(@valid_attrs)
      assert ticket.access == 42
      assert ticket.performance_id == 42
      assert ticket.price == Decimal.new("120.5")
      assert ticket.status == 42
      assert ticket.user_id == 42
    end

    test "create_ticket/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Events.create_ticket(@invalid_attrs)
    end

    test "update_ticket/2 with valid data updates the ticket" do
      ticket = ticket_fixture()
      assert {:ok, ticket} = Events.update_ticket(ticket, @update_attrs)
      assert %Ticket{} = ticket
      assert ticket.access == 43
      assert ticket.performance_id == 43
      assert ticket.price == Decimal.new("456.7")
      assert ticket.status == 43
      assert ticket.user_id == 43
    end

    test "update_ticket/2 with invalid data returns error changeset" do
      ticket = ticket_fixture()
      assert {:error, %Ecto.Changeset{}} = Events.update_ticket(ticket, @invalid_attrs)
      assert ticket == Events.get_ticket!(ticket.id)
    end

    test "delete_ticket/1 deletes the ticket" do
      ticket = ticket_fixture()
      assert {:ok, %Ticket{}} = Events.delete_ticket(ticket)
      assert_raise Ecto.NoResultsError, fn -> Events.get_ticket!(ticket.id) end
    end

    test "change_ticket/1 returns a ticket changeset" do
      ticket = ticket_fixture()
      assert %Ecto.Changeset{} = Events.change_ticket(ticket)
    end
  end
end
