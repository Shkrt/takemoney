defmodule Takemoney.ShoppingCartTest do
  use Takemoney.DataCase
  alias Takemoney.Events.ShoppingCart
  alias Takemoney.Events
  alias Takemoney.Accounts

  @user_attrs %{
    email: "user@example.com",
    first_name: "Ui",
    last_name: "La",
    password: "11111111"
  }

  @event_attrs %{
    name: "Evt",
    description: "Ui",
    image_url: "http://placehold.it/300x300"
  }

  defp perf_attrs do
    {:ok, start_time} = NaiveDateTime.new(2017, 1, 1, 23, 59, 59)
    {:ok, end_time} = NaiveDateTime.new(2017, 2, 1, 23, 59, 59)
    %{
      start_time: start_time,
      event_id: event_fixture.id,
      end_time: end_time
    }
  end

  defp user_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(@user_attrs)
      |> Accounts.create_user()
    user
  end

  defp performance_fixture(attrs \\ %{}) do
    {:ok, perf} =
      attrs
      |> Enum.into(perf_attrs)
      |> Events.create_performance()
    perf
  end

  defp event_fixture(attrs \\ %{}) do
    {:ok, event} =
      attrs
      |> Enum.into(@event_attrs)
      |> Events.create_event()
    event
  end

  defp create_tickets(attrs) do
    for _ <- 0..2 do
      Events.create_ticket(attrs)
    end
  end

  describe "shopping cart" do
    test "total_cost calculates total for entire cart" do
      user = user_fixture
      performance = performance_fixture
      create_tickets(%{user_id: user.id, performance_id: performance.id, price: 500})
      cart = %ShoppingCart{ticket_count: 2, user: user}
      assert ShoppingCart.total_cost(cart) == 1000
    end
  end
end

