defmodule TakemoneyWeb.Router do
  use TakemoneyWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug TakemoneyWeb.Plugs.CurrentUser
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", TakemoneyWeb do
    pipe_through :browser # Use the default browser stack

    get "/", EventController, :index
    resources "/events", EventController, only: [:show]
    resources "/shopping_cart", ShoppingCartController, singleton: true
    resources "/sessions", SessionController, only: [:new, :create], singleton: true
    delete "/sessions", SessionController, :destroy
  end

  # Other scopes may use custom stacks.
  # scope "/api", TakemoneyWeb do
  #   pipe_through :api
  # end
end
