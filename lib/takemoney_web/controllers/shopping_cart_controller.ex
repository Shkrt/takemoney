defmodule TakemoneyWeb.ShoppingCartController do
  use TakemoneyWeb, :controller
  alias Takemoney.Events
  alias Workflows.AddsToCart
  alias Takemoney.Events.ShoppingCart
  alias TakemoneyWeb.Plugs.CurrentUser

  def update(conn, %{"performance_id" => performance_id, "" => %{"ticket_count" => ticket_count}}) do
    performance = Events.get_performance_with_event!(performance_id)
    workflow = AddsToCart.run(%{user: CurrentUser.get(conn), performance: performance, count: ticket_count})

    case workflow do
      :ok ->
        redirect(conn, to: shopping_cart_path(conn, :show))
      _ ->
        redirect(conn, to: event_path(conn, :show, performance.event))
    end
  end

  def show(conn, _params) do
    cart = %ShoppingCart{user: CurrentUser.get(conn)}
    render conn, "show.html", cart: cart, events: ShoppingCart.events(cart)
  end
end

