defmodule TakemoneyWeb.SessionController do
  use TakemoneyWeb, :controller
  alias Takemoney.Accounts
  alias TakemoneyWeb.Plugs.CurrentUser

  def new(conn, _params) do
    render(conn, "new.html")
  end

  def create(conn, %{"credentials" => %{"email" => email, "password" => password}}) do
    case Accounts.authenticate_user(email, password) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "Welcome to Takemoney")
        |> CurrentUser.set(user)
        |> redirect(to: event_path(conn, :index))
      {:error, _} ->
        conn
        |> put_flash(:error, "Unable to sign in")
        |> render("new.html")
    end
  end

  def destroy(conn, _params) do
    conn
    |> put_flash(:info, "You were logged out")
    |> CurrentUser.forget
    |> redirect(to: session_path(conn, :new))
  end
end
