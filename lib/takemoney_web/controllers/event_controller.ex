defmodule TakemoneyWeb.EventController do
  use TakemoneyWeb, :controller
  alias Takemoney.Events
  plug :ensure_current_user

  def index(conn, _params) do
    events = Events.list_events
    render conn, "index.html", events: events
  end

  def show(conn, %{"id" => id}) do
    event = Events.get_event_with_performances!(id)
    render conn, "show.html", event: event
  end
end

