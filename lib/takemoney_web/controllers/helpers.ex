defmodule TakemoneyWeb.Controllers.Helpers do
  import Phoenix.Controller
  alias TakemoneyWeb.Plugs

  def ensure_current_user(conn, _opts) do
    if Plugs.CurrentUser.get(conn) do
      conn
    else
      conn
      |> put_flash(:error, "Sign in or Sign up before you continue")
      |> redirect(to: "/sessions/new")
    end
  end
end
