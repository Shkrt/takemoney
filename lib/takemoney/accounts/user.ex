defmodule Takemoney.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias Takemoney.Accounts.User

  schema "users" do
    field :email, :string
    field :first_name, :string
    field :last_name, :string
    field :password_hash, :string
    field :password, :string, virtual: true

    timestamps()
  end

  @doc false
  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:email, :first_name, :last_name, :password])
    |> validate_required([:email, :first_name, :last_name, :password])
    |> validate_length(:password, min: 6)
    |> unique_constraint(:email)
    |> put_password_hash()
  end

  defp put_password_hash(changeset=%{valid?: true}) do
    password = get_change(changeset, :password)
    change(changeset, Comeonin.Bcrypt.add_hash(password))
  end

  defp put_password_hash(changeset) do
    changeset
  end

  def check_password(user, password) do
    Comeonin.Bcrypt.check_pass(user, password)
  end
end
