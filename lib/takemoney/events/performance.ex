defmodule Takemoney.Events.Performance do
  use Ecto.Schema
  import Ecto.Changeset
  alias Takemoney.Events.Performance


  schema "performances" do
    field :end_time, :naive_datetime
    field :start_time, :naive_datetime
    belongs_to :event, Takemoney.Events.Event
    has_many :tickets, Takemoney.Events.Ticket

    timestamps()
  end

  @doc false
  def changeset(%Performance{} = performance, attrs) do
    performance
    |> cast(attrs, [:event_id, :start_time, :end_time])
    |> validate_required([:event_id, :start_time, :end_time])
  end
end
