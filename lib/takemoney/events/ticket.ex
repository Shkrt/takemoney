defmodule Takemoney.Events.Ticket do
  use Ecto.Schema
  import Ecto.Changeset
  alias Takemoney.Events.Ticket

  schema "tickets" do
    field :access, :integer
    field :price, :decimal
    field :status, :string
    belongs_to :user, Takemoney.Accounts.User
    belongs_to :performance, Takemoney.Events.Performance
    has_one :event, through: [:performance, :event]

    timestamps()
  end

  @doc false
  def changeset(%Ticket{} = ticket, attrs) do
    ticket
    |> cast(attrs, [:user_id, :performance_id, :status, :access, :price])
    |> validate_required([:performance_id, :price])
  end
end
