defmodule Takemoney.Events.ShoppingCart do
  alias Takemoney.Events.ShoppingCart
  alias Takemoney.Repo
  import Ecto.Query
  defstruct [:user, :performance_id, :ticket_count]

  def tickets(cart = %ShoppingCart{}) do
    Takemoney.Events.tickets_in_cart(cart.user)
  end

  def events(cart = %ShoppingCart{}) do
    tickets_query(cart)
    |> preload(:event)
    |> Repo.all
    |> Enum.map(&(&1.event))
    |> Enum.uniq(&(&1.id))
    |> Enum.sort_by(&(&1.name))
  end

  def performance_count(cart = %ShoppingCart{}) do
    tickets_query(cart)
    |> group_by([u], u.performance_id)
    |> select([t], {t.performance_id, count(t.id)})
    |> Repo.all
  end

  def tickets_by_performance(cart = %ShoppingCart{}) do
    tickets(cart) |> Enum.group_by(&(&1.performance_id))
  end

  def performances_for(cart = %ShoppingCart{}, event) do
    ids = tickets_query(cart) |> select([t], t.performance_id) |> Repo.all |> Enum.uniq
    Performance
    |> where([p], p.id in ^ids)
    |> where([p], p.event_id == ^event.id)
    |> order_by([p], p.start_time)
    |> Repo.all
  end

  def subtotal_for(performance, cart = %ShoppingCart{}) do
    tickets_query(cart)
    |> where([u], u.performance_id == ^performance.id)
    |> select([u], sum(u.price))
    |> Repo.one
  end

  def total_cost(cart = %ShoppingCart{}) do
    tickets_query(cart) |> select([u], sum(u.price)) |> Repo.one
  end

  defp tickets_query(cart = %ShoppingCart{}) do
    Takemoney.Events.tickets_in_cart_query(cart.user)
  end

end
