defmodule Takemoney.Events.Event do
  use Ecto.Schema
  import Ecto.Changeset
  alias Takemoney.Events.Event


  schema "events" do
    field :description, :string
    field :image_url, :string
    field :name, :string
    has_many :performances, Takemoney.Events.Performance

    timestamps()
  end

  @doc false
  def changeset(%Event{} = event, attrs) do
    event
    |> cast(attrs, [:name, :description, :image_url])
    |> validate_required([:name, :description, :image_url])
  end
end
