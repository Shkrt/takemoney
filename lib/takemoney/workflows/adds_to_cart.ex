defmodule Workflows.AddsToCart do
  alias Takemoney.Events

  def run(%{user: user, performance: performance, count: count}) do
    tickets = Events.unsold_tickets(performance.id, count)

    if Enum.count(tickets) == count do
      place_tickets(tickets, user)
      validate_tickets(tickets)
    else
      :error
    end
  end

  defp place_tickets(tickets, user) do
    tickets |> Enum.each(&(Events.ticket_place_in_cart_for(&1, user)))
  end

  defp validate_tickets(tickets) do
    if Enum.all?(tickets, &(&1.valid?)) do
      :ok
    else
      :error
    end
  end
end
