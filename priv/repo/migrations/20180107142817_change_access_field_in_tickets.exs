defmodule Takemoney.Repo.Migrations.ChangeAccessFieldInTickets do
  use Ecto.Migration

  def change do
    alter table(:tickets) do
      modify :access, :string
    end
  end
end
