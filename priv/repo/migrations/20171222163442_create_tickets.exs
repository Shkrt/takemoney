defmodule Takemoney.Repo.Migrations.CreateTickets do
  use Ecto.Migration

  def change do
    create table(:tickets) do
      add :user_id, :integer
      add :performance_id, :integer
      add :status, :integer
      add :access, :integer
      add :price, :decimal

      timestamps()
    end

  end
end
