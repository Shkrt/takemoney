defmodule Takemoney.Repo.Migrations.ChangeStatusFieldInTickets do
  use Ecto.Migration

  def change do
    alter table(:tickets) do
      modify :status, :string
    end
  end
end
