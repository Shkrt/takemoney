defmodule Takemoney.Repo.Migrations.CreatePerformances do
  use Ecto.Migration

  def change do
    create table(:performances) do
      add :event_id, :integer
      add :start_time, :naive_datetime
      add :end_time, :naive_datetime

      timestamps()
    end

  end
end
