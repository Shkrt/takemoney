defmodule Takemoney.Repo.Migrations.CreateEvents do
  use Ecto.Migration

  def change do
    create table(:events) do
      add :name, :string
      add :description, :text
      add :image_url, :string

      timestamps()
    end

  end
end
