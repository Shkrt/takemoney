# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

config :phoenix, :template_engines,
  slim: PhoenixSlime.Engine,
  slime: PhoenixSlime.Engine

# General application configuration
config :takemoney,
  ecto_repos: [Takemoney.Repo]

# Configures the endpoint
config :takemoney, TakemoneyWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "lDBhYeYRA1XuJCeRlAbsGomTKvX/ma7aabgPwlq7NXULE1Il0de+VPj/bi+gRA+z",
  render_errors: [view: TakemoneyWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Takemoney.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
