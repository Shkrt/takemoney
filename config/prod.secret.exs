use Mix.Config

# In this file, we keep production configuration that
# you'll likely want to automate and keep away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or yourself later on).
config :takemoney, TakemoneyWeb.Endpoint,
  secret_key_base: "x25ANoqFxF1mNR5lPGCixEUqRNAoBMFEKHm9UOgQvZl6tbl0yRdTi2FKPLJW7S9K"

# Configure your database
config :takemoney, Takemoney.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "takemoney_prod",
  pool_size: 15
